package com.claurendeau.graphic;

import com.claurendeau.utils.IAngle;

import java.awt.Point;
import java.awt.Graphics;

public class GraphicChopstick extends Graphic2D {

    private Point coordStart;
    private Point coordEnd;

    public GraphicChopstick(int chopstickId, Point coordCenter, Point coordStart, Point coordEnd) {
        super(chopstickId, coordCenter);
        angle = IAngle.ANGLE_PLACEMENT_PLATE * graphicId + IAngle.ANGLE_PLACEMENT_CHOPSTICK;
        this.coordStart = new Point(rotate(coordStart, coordCenter, angle));
        this.coordStart.y += 15;

        this.coordEnd = new Point(rotate(coordEnd, coordCenter, angle));
        this.coordEnd.y += 15;
    }

    public void draw(Graphics graphic) {
        graphic.setColor(color);
        graphic.drawLine(coordStart.x, coordStart.y, coordEnd.x, coordEnd.y);
    }


    public Point getCoordStart() {
        return coordStart;
    }

    public Point getCoordEnd() {
        return coordEnd;
    }

}
  
