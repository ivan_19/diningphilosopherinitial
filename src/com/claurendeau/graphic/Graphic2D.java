package com.claurendeau.graphic;

import com.claurendeau.utils.IAngle;
import com.claurendeau.utils.IColor;

import java.awt.*;

public abstract class Graphic2D {

    protected int graphicId;
    protected int angle;
    protected Color color;
    protected Point coordCenter;
    protected int philosopherId;

    public Graphic2D(int graphicId, Point coordCenter) {
        this.graphicId = graphicId;
        this.coordCenter = coordCenter;
        this.philosopherId = IColor.COLOR_BLACK;
        setColor(philosopherId);
    }

    protected abstract void draw(Graphics graphic);

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Point getCoordCenter() {
        return coordCenter;
    }

    public void setCoordCenter(Point coordCenter) {
        this.coordCenter = coordCenter;
    }

    public int getPhilosopherId() {
        return philosopherId;
    }

    public void setPhilosopherId(int philosopherId) {
        this.philosopherId = philosopherId;
    }

    public void setColor(int philosopherId) {
        this.philosopherId = philosopherId;

        if (philosopherId == IColor.COLOR_BLACK) {
            this.color = Color.black;
        } else if (philosopherId == IColor.COLOR_RED) {
            this.color = Color.red;
        } else if (philosopherId == IColor.COLOR_BLUE) {
            this.color = Color.blue;
        } else if (philosopherId == IColor.COLOR_GREEN) {
            this.color = Color.green;
        } else if (philosopherId == IColor.COLOR_YELLOW) {
            this.color = Color.yellow;
        } else if (philosopherId == IColor.COLOR_WHITE) {
            this.color = Color.white;
        }
    }

    public static Point rotate(Point coordPoint, Point coordCenter, int angle) {
        Point point = new Point();
        double radians = Math.PI / (IAngle.ANGLE_PLAT / angle);

        point.x = (int)
                (coordPoint.x * Math.cos(radians)
                        - coordPoint.y * Math.sin(radians)
                        - Math.cos(radians) * coordCenter.x
                        + Math.sin(radians) * coordCenter.y
                        + coordCenter.x);

        point.y = (int)
                (coordPoint.x * Math.sin(radians)
                        + coordPoint.y * Math.cos(radians)
                        - Math.sin(radians) * coordCenter.x
                        - Math.cos(radians) * coordCenter.y
                        + coordCenter.y);
        return point;
    }
}
