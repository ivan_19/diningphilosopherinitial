package com.claurendeau.graphic;

import com.claurendeau.utils.IAngle;
import com.claurendeau.utils.IConstants;

import java.awt.Point;
import java.awt.Graphics;

public class GraphicPlate extends Graphic2D {

    private Point coordonnee;
    private int size;

    public GraphicPlate(int plateId, Point coordCenter, Point coord, int size) {
        super(plateId, coordCenter);
        this.size = size;
        angle = IAngle.ANGLE_PLACEMENT_PLATE * graphicId;
        this.coordonnee = new Point(rotate(coord, coordCenter, angle));
        this.coordonnee.x -= IConstants.COORD_TRANSLATE_ASBCISSE;
        this.coordonnee.y += IConstants.COORD_TRANSLATE_ORDONNEE;
    }

    public void draw(Graphics graphic) {
        graphic.setColor(color);
        graphic.fillOval(coordonnee.x, coordonnee.y, size, size);
    }

}
  
