package com.claurendeau.graphic;

import com.claurendeau.utils.*;

import java.awt.Point;
import java.awt.Frame;
import java.awt.Color;
import java.awt.Graphics;

public class GraphicTable extends Frame {

    private Point coordCenter;
    private Graphic2D graphicPlates[];
    private Graphic2D graphicChopsticks[];

    public GraphicTable() {
        super();

        addWindowListener(new GraphicTableAdapter());
        setTitle("Dining Philosophers");
        setSize(IConstants.SIZE_WINDOW_LISTENER, IConstants.SIZE_WINDOW_LISTENER);
        setBackground(Color.darkGray);

        coordCenter = new Point(getSize().width / IConstants.NUMBER_TWO
                                    , getSize().height / IConstants.NUMBER_TWO);

        graphicPlates = new GraphicPlate[IConstants.MAX_NUMBER_PHILOSOPHERS];
        graphicChopsticks = new GraphicChopstick[IConstants.MAX_NUMBER_PHILOSOPHERS];
        fillGraphicPlates();
        fillGraphicChopsticks();
        show();
        setResizable(false);
    }

    private void fillGraphicPlates(){
        for (int i = 0; i < IConstants.MAX_NUMBER_PHILOSOPHERS; i++) {
            graphicPlates[i] =
                    new GraphicPlate(i, coordCenter,
                            new Point(coordCenter.x, coordCenter.y
                                    - IAngle.ANGLE_INITIAL_START_UTENSILS), IConstants.SIZE_TABLE);
        }
    }

    private void fillGraphicChopsticks(){
        for (int i = 0; i < IConstants.MAX_NUMBER_PHILOSOPHERS; i++) {
            graphicChopsticks[i] = new GraphicChopstick(i, coordCenter,
                    new Point(coordCenter.x, coordCenter.y - IAngle.ANGLE_INITIAL_START_UTENSILS),
                    new Point(coordCenter.x, coordCenter.y - IAngle.ANGLE_INITIAL_END_UTENSILS));
        }
    }

    public void isHungry(int philosopherId) {
        graphicPlates[philosopherId].setColor(philosopherId);
        repaint();
    }

    public void isThinking(int philosopherId) {
        graphicPlates[philosopherId].setColor(IColor.COLOR_BLACK);
        repaint();
    }

    public void takeChopstick(int philosopherId, int chopstickId) {
        graphicChopsticks[chopstickId].setColor(philosopherId);
        repaint();
    }

    public void releaseChopstick(int philosopherId, int chopstickId) {
        graphicChopsticks[chopstickId].setColor(IColor.COLOR_BLACK);
        repaint();
    }

    public void paint(Graphics graphic) {
        for (int i = 0; i < IConstants.MAX_NUMBER_PHILOSOPHERS; i++) {
            graphicPlates[i].draw(graphic);
            graphicChopsticks[i].draw(graphic);
        }
    }

    public Graphic2D[] getGraphicChopsticks() {
        return graphicChopsticks;
    }

    public Graphic2D[] getGraphicPlates() {
        return graphicPlates;
    }
}
