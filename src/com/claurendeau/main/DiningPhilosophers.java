package com.claurendeau.main;

import com.claurendeau.entity.Room;

public class DiningPhilosophers {

    public static void main(String args[]) {
        Room room = new Room();
        room.startDinnerPhilosophers();
    }
}
