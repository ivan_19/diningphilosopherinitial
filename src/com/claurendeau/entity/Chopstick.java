package com.claurendeau.entity;

public class Chopstick {

    private int chopstickId;
    private boolean isFree;

    public Chopstick(int chopstickId) {
        this.chopstickId = chopstickId;
        isFree = true;
    }

    public synchronized void take() {
        while (!isFree) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("com.claurendeau.entity.Chopstick Error - ");
            }
        }
        isFree = false;
    }

    public synchronized void release() {
        notifyAll();
        isFree = true;
    }

    public int getChopstickId() {
        return chopstickId;
    }

    public void setChopstickId(int chopstickId) {
        this.chopstickId = chopstickId;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
