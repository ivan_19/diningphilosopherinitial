package com.claurendeau.entity;

import com.claurendeau.graphic.GraphicTable;
import com.claurendeau.utils.ITime;

public class Philosopher extends Thread {

    private int PhilosopherId;
    private GraphicTable table;
    private Chopstick leftChopstick;
    private Chopstick rightChopstick;

    public Philosopher(int PhilosopherId, GraphicTable table, Chopstick leftChopstick, Chopstick rightChopstick) {
        this.PhilosopherId = PhilosopherId;
        this.table = table;
        this.leftChopstick = leftChopstick;
        this.rightChopstick = rightChopstick;
        setName(Philosopher.class.getName() + " " + PhilosopherId);
    }

    private void sleepTime(int time) {
        try {
            sleep((long) (Math.random() * time));
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    public void run() {
        for (;;) {
            think();
            hungry();

            takeLeftChopstick();
            sleepTime(ITime.TIME_NEXT_CHOPSTICK);

            if(!isDeadLock()){
                releaseLeftChopstick();
                continue;
            }

            takeRightChopstick();
            eat();
            releaseChopsticks();
        }
    }

    private boolean isDeadLock(){
        return rightChopstick.isFree();
    }

    private void takeRightChopstick() {
        System.out.println(getName() + " wants rightChopstick chopstick");
        rightChopstick.take();
        table.takeChopstick(PhilosopherId, rightChopstick.getChopstickId());
        System.out.println(getName() + " got rightChopstick chopstick");
    }

    private void takeLeftChopstick() {
        System.out.println(getName() + " wants leftChopstick chopstick");
        leftChopstick.take();
        table.takeChopstick(PhilosopherId, leftChopstick.getChopstickId());
        System.out.println(getName() + " got leftChopstick chopstick");
    }

    private void think() {
        table.isThinking(PhilosopherId);
        System.out.println(getName() + " thinks");
        sleepTime(ITime.TIME_THINK_MAX);
        System.out.println(getName() + " finished thinking");
    }

    private void hungry() {
        System.out.println(getName() + " is hungry");
        table.isHungry(PhilosopherId);
    }

    private void eat() {
        System.out.println(getName() + " eats");
        sleepTime(ITime.TIME_EAT_MAX);
        System.out.println(getName() + " finished eating");
    }

    private void releaseChopsticks() {
        releaseLeftChopstick();
        releaseRightChopstick();
    }

    private void releaseLeftChopstick() {
        table.releaseChopstick(PhilosopherId, leftChopstick.getChopstickId());
        leftChopstick.release();
        System.out.println(getName() + " released leftChopstick chopstick");
    }

    private void releaseRightChopstick() {
        table.releaseChopstick(PhilosopherId, rightChopstick.getChopstickId());
        rightChopstick.release();
        System.out.println(getName() + " released rightChopstick chopstick");
    }

}
