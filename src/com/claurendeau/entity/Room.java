package com.claurendeau.entity;

import com.claurendeau.utils.IConstants;
import com.claurendeau.graphic.GraphicTable;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private GraphicTable table;
    private List<Philosopher> philosophers = new ArrayList<Philosopher>();
    private List<Chopstick> chopsticks = new ArrayList<Chopstick>();

    public Room() {
        table = new GraphicTable();
        fillChopsticks();
        attribuateChopsticksToPhilosophers();
    }

    private void fillChopsticks(){
        for (int i = 0; i < IConstants.MAX_NUMBER_PHILOSOPHERS; i++)
            chopsticks.add(new Chopstick(i));
    }

    private void attribuateChopsticksToPhilosophers(){
        for (int i = 0; i < chopsticks.size(); i++) {

            Chopstick leftChopstick = chopsticks.get(i);
            Chopstick rightChopstick = chopsticks.get(getRightChoptickId(i));

            if(i == chopsticks.size() + 1)
                philosophers.add(new Philosopher(i, table, leftChopstick, rightChopstick));
            else
                philosophers.add(new Philosopher(i, table, rightChopstick, leftChopstick));
        }
    }

    private int getRightChoptickId(int id){
        return (id == 0) ? chopsticks.size() - 1 : id - 1;
    }

    public void startDinnerPhilosophers(){
        for (int i = 0; i < IConstants.MAX_NUMBER_PHILOSOPHERS; i++)
            philosophers.get(i).start();
    }
}
