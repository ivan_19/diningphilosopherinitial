package com.claurendeau.tests;

import com.claurendeau.graphic.Graphic2D;
import com.claurendeau.graphic.GraphicChopstick;
import com.claurendeau.graphic.GraphicTable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Graphic2DTest {

    GraphicTable graphicTable;
    GraphicChopstick graphicChopstick;
    int chopstickId;

    @Before
    public void setUp() throws Exception {
        chopstickId = 2;
        graphicTable = new GraphicTable();
        graphicChopstick = (GraphicChopstick) graphicTable.getGraphicChopsticks()[chopstickId];
    }

    @Test
    public void rotateTest() {
        GraphicChopstick tmpGraphicChopstick = graphicChopstick;
        Graphic2D.rotate(graphicChopstick.getCoordStart(), graphicChopstick.getCoordCenter(), graphicChopstick.getAngle());
        Graphic2D.rotate(graphicChopstick.getCoordStart(), graphicChopstick.getCoordCenter(), graphicChopstick.getAngle());
        assertThat(graphicChopstick).isEqualToComparingFieldByFieldRecursively(tmpGraphicChopstick);
    }

    @After
    public void tearDown() throws Exception {
        graphicTable = null;
        graphicChopstick = null;
    }
}