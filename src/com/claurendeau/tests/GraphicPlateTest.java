package com.claurendeau.tests;

import com.claurendeau.graphic.GraphicPlate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

import static org.assertj.core.api.Assertions.assertThat;

public class GraphicPlateTest {

    int chopstickId;
    int philosopherId;
    JFrame jFrame;
    Graphics graphic;
    boolean isVisble;
    GraphicPlate graphicPlate;

    @Before
    public void setUp() throws Exception {
        philosopherId = 1;
        graphicPlate = new GraphicPlate(0, new Point(), new Point(), 10);
        jFrame = new JFrame();
        isVisble = true;
        jFrame.setVisible(isVisble);
        graphic = jFrame.getGraphics();
    }

    @Test
    public void setColorText() {
        graphicPlate.setColor(philosopherId);
        assertThat(graphicPlate.getColor()).isSameAs(Color.blue);
    }

    @Test
    public void drawText() {
        graphicPlate.setColor(philosopherId);
        graphicPlate.draw(graphic);
        assertThat(graphic.getColor()).isSameAs(Color.blue);
    }

    @After
    public void tearDown() throws Exception {
        graphicPlate = null;
        jFrame = null;
    }
}