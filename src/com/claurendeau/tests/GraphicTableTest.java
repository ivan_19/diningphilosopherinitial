package com.claurendeau.tests;

import com.claurendeau.graphic.GraphicChopstick;
import com.claurendeau.graphic.GraphicPlate;
import com.claurendeau.graphic.GraphicTable;
import com.claurendeau.utils.IColor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

import static org.assertj.core.api.Assertions.assertThat;

public class GraphicTableTest {

    int chopstickId;
    int philosopherId;
    JFrame jFrame;
    Graphics graphic;
    GraphicTable graphicTable;
    GraphicPlate graphicPlate;
    GraphicChopstick graphicChopstick;
    Boolean isVisble;

    @Before
    public void setUp() throws Exception {
        chopstickId = 1;
        philosopherId = 3;
        graphicTable = new GraphicTable();
        jFrame = new JFrame();
        isVisble = true;
        jFrame.setVisible(isVisble);
        graphic = jFrame.getGraphics();
        isVisble = true;
        graphicPlate = (GraphicPlate) graphicTable.getGraphicPlates()[philosopherId];
        graphicChopstick = (GraphicChopstick) graphicTable.getGraphicChopsticks()[chopstickId];
    }

    @Test
    public void takeChopstickTest() {
        graphicTable.takeChopstick(philosopherId, chopstickId);
        GraphicChopstick tmpGraphicChopstick = new GraphicChopstick(chopstickId, graphicChopstick.getCoordCenter(),
                graphicChopstick.getCoordStart(), graphicChopstick.getCoordEnd());
        tmpGraphicChopstick.setColor(philosopherId);
        assertThat(graphicChopstick.getColor()).isSameAs(tmpGraphicChopstick.getColor());
    }

    @Test
    public void releaseChopstickTest() {
        graphicTable.releaseChopstick(IColor.COLOR_BLACK, chopstickId);
        assertThat(graphicChopstick.getColor()).isEqualTo(new GraphicChopstick(chopstickId,
                                                        graphicChopstick.getCoordCenter(),
                                                        graphicChopstick.getCoordStart(),
                                                        graphicChopstick.getCoordEnd()).getColor());

    }

    @Test
    public void isHungryTest() {
        graphicTable.isHungry(philosopherId);
        assertThat(graphicPlate.getColor()).isSameAs(Color.yellow);
    }

    @Test
    public void isThinkingTest() {
        graphicTable.isThinking(philosopherId);
        assertThat(graphicPlate.getColor()).isSameAs(Color.black);
    }

    @Test
    public void getChopsTest() {
        assertThat(graphicTable.getGraphicChopsticks()).hasSize(5);
    }

    @Test
    public void getPlatesTest() {
        assertThat(graphicTable.getGraphicPlates()).hasSize(5);
    }

    @Test
    public void paintTest() {
        graphicTable.getGraphicPlates()[philosopherId].setColor(3);
        graphicTable.paint(graphic);
        assertThat(graphicTable.getGraphicPlates()[philosopherId].getColor()).isSameAs(Color.yellow);
    }

    @After
    public void tearDown() throws Exception {
        graphicTable = null;
        graphicPlate = null;
        graphicChopstick = null;
        jFrame = null;
        isVisble = null;
    }
}