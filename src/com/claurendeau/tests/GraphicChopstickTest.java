package com.claurendeau.tests;

import com.claurendeau.graphic.GraphicPlate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

import static org.assertj.core.api.Assertions.assertThat;

public class GraphicChopstickTest {

    int philosopherID;
    JFrame jFrame;
    Graphics graphic;
    boolean isVisble;
    GraphicPlate graphicPlate;

    @Before
    public void setUp() throws Exception {
        philosopherID = 2;
        graphicPlate = new GraphicPlate(0, new Point(), new Point(), 10);
        jFrame = new JFrame();
        isVisble = true;
        jFrame.setVisible(isVisble);
        graphic = jFrame.getGraphics();
    }

    @Test
    public void setColor() {
        graphicPlate.setColor(philosopherID);
        assertThat(graphicPlate.getColor()).isSameAs(Color.green);
    }

    @Test
    public void drawText() {
        graphicPlate.setColor(philosopherID);
        graphicPlate.draw(graphic);
        assertThat(graphic.getColor()).isSameAs(Color.green);
    }

    @After
    public void tearDown() throws Exception {
        graphicPlate = null;
        jFrame = null;
    }
}