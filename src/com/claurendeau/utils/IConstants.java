package com.claurendeau.utils;

public interface IConstants {

    int NUMBER_TWO = 2;
    int MAX_NUMBER_PHILOSOPHERS = 5;

    int SIZE_TABLE = 20;
    int SIZE_WINDOW_LISTENER = 200;

    int COORD_TRANSLATE_ORDONNEE = 5;
    int COORD_TRANSLATE_ASBCISSE = 10;
}
