package com.claurendeau.utils;

public interface IColor {
    int COLOR_BLACK = -1;
    int COLOR_RED = 0;
    int COLOR_BLUE = 1;
    int COLOR_GREEN = 2;
    int COLOR_YELLOW = 3;
    int COLOR_WHITE = 4;
}
