package com.claurendeau.utils;

public interface ITime {

    int TIME_THINK_MAX = 5000;
    int TIME_NEXT_CHOPSTICK = 100;
    int TIME_EAT_MAX = 2000;
}
